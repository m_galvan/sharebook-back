# API ShareBook

Une API de livres par auteurs et catégories.
Cette API sera utilisée pour créer une application web sur le thème du livre.

### Installation et utilisation

##### Getting Started
1. Cloner le projet
2. Installer les dépendances avec ```composer install```
3. Lancer en ligne de commandes ```symfony server:start``` 

##### Base de donnée
1. Dans le fichier _.env.local_, entrer les informations de connexion à la base de donnée.

        DATABASE_URL=mysql://user:password@127.0.0.1:3306/nom_de_la_bdd

2. Etablir les tables avec ```php bin/console make:migration```
3. Puis, ```php bin/console doctrine:migrations:migrate```

Possibilité d'insérer les fixtures avec ```php bin/console doctrine:fixture:load```